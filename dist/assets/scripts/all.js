'use strict';

var dictionaryForAutocomplete = [{
  text: 'название элемента1',
  pattern: ['синоним1', 'синоним2']
}, {
  text: 'название элемента2',
  pattern: ['синоним1', 'синоним2']
}, {
  text: 'название элемента3',
  pattern: ['синоним1', 'синоним2']
}];

var optionsForAutocomplete = {
  resultsClass: 'autocomplete',
  selectedClass: 'autocomplete__item'
};

var selectAll = {
  deselect: function deselect(target) {
    $("[data-selectTarget=" + target + "]").prop('checked', false);
  },
  select: function select(target) {
    $("[data-selectTarget=" + target + "]").prop('checked', true);
  }
};

var uploadFile = {
  formClass: '.js-input-file-form',
  chooseBlockClass: '.js-input-file-choose',
  dataBlockClass: '.js-input-file-upload-info',
  fileNameClass: '.js-input-file-name',
  disableButtonClass: 'files-form__submit_disabled',
  deleteUploadedFile: function deleteUploadedFile(input) {
    input[0].files[0] = [];
  },
  setDataToFields: function setDataToFields(input, file) {
    input.closest(this.formClass).find(this.fileNameClass).html(file.name);
  },
  removeDataFromFields: function removeDataFromFields(input) {
    input.closest(this.formClass).find(this.fileNameClass).html('');
  },
  disableButton: function disableButton(input) {
    input.closest(this.formClass).find('button').addClass(this.disableButtonClass);
  },
  enableButton: function enableButton(input) {
    input.closest(this.formClass).find('button').removeClass(this.disableButtonClass);
  },
  showUploadInput: function showUploadInput(input) {
    input.closest(this.formClass).find(this.chooseBlockClass).show();
    input.closest(this.formClass).find(this.dataBlockClass).hide();
  },
  hideUploadInput: function hideUploadInput(input) {
    input.closest(this.formClass).find(this.chooseBlockClass).hide();
    input.closest(this.formClass).find(this.dataBlockClass).show();
  }
};

var cloneInput = {
  rowClass: '.js-input-add-row',
  add: function add(input) {
    var parent = input.closest(this.rowClass);
    var newInputRow = parent.clone();

    newInputRow.find('input').val('');
    newInputRow.find('.js-input-add-field').removeClass().addClass('input__remove-icon icon icon-remove js-input-remove-field');
    newInputRow.insertAfter(parent);
  },
  remove: function remove(input) {
    var parent = input.closest(this.rowClass);
    parent.remove();
  }
};

var tabs = {
  activeClass: 'tabs__header-item_active',
  tabClass: '.js-tabs-header-item',
  bodyClass: '.js-tabs-body',
  hideTabs: function hideTabs() {
    this.$tab.find(this.bodyClass).hide();
  },
  showTab: function showTab(target) {
    this.$tab.find('.tabs__body_' + target).fadeIn();
  },
  removeActiveClass: function removeActiveClass() {
    this.$tab.find(this.tabClass).removeClass(this.activeClass);
  },
  addActiveClass: function addActiveClass($target) {
    $target.addClass(this.activeClass);
  },
  enter: function enter($target) {
    this.$tab = $target.closest('.js-tabs');
    var tab = $target.attr('data-tab');
    if (!$target.hasClass(this.activeClass)) {
      this.removeActiveClass();
      this.addActiveClass($target);
      this.hideTabs();
      this.showTab(tab);
    } else {
      return false;
    }
  }
};

var accordion = {
  mainClass: '.js-widjet-accordion',
  hideClass: 'widjet__element_closed',
  arrowHideClass: 'widjet__element-arrow_hide',
  show: function show($target) {
    $target.closest(this.mainClass).removeClass(this.hideClass);
  },
  hide: function hide($target) {
    $target.closest(this.mainClass).addClass(this.hideClass);
  },
  init: function init($target) {
    if ($target.hasClass(this.arrowHideClass)) {
      $target.removeClass(this.arrowHideClass);
      this.show($target);
    } else {
      $target.addClass(this.arrowHideClass);
      this.hide($target);
    }
  }
};

var beautyArrow = {
  mainClass: '.js-beauty-arrow',
  focusClass: 'input__beauty-arrow_focus',
  focus: function focus($target) {
    $target.siblings(this.mainClass).addClass(this.focusClass);
  },
  blur: function blur($target) {
    $target.siblings(this.mainClass).removeClass(this.focusClass);
  }
};

var popup = {
  $popup: $('.js-popup'),
  popupItemClass: '.js-popup-item',
  showPopup: function showPopup(target) {
    var $popup = $(this.popupItemClass + '-' + target);
    this.$popup.fadeIn(300);
    $popup.fadeIn(300);
  },
  hidePopups: function hidePopups() {
    this.$popup.fadeOut(300);
    $(this.popupItemClass).fadeOut(300);
  }
};

$(function () {
  $('.js-select').each(function () {
    var $this = $(this);
    var numberOfOptions = $this.children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
      'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
      $('<li />', {
        text: $this.children('option').eq(i).text(),
        rel: $this.children('option').eq(i).val()
      }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function (e) {
      e.stopPropagation();
      $('div.select-styled.active').not(this).each(function () {
        $(this).removeClass('active').next('ul.select-options').hide();
      });
      $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function (e) {
      e.stopPropagation();
      $styledSelect.text($(this).text()).removeClass('active');
      $this.val($(this).attr('rel'));
      $list.hide();
    });

    $(document).click(function () {
      $styledSelect.removeClass('active');
      $list.hide();
    });
  });

  $(".js-resizable").resizable();

  $('.js-sortable').sortable({
    revert: true
  });

  $(".js-select-all").change(function () {
    var target = $(this).attr("data-selectId");
    if ($(this)[0].checked) {
      selectAll.select(target);
    } else {
      selectAll.deselect(target);
    }
  });

  $(".js-input-file").change(function () {
    uploadFile.setDataToFields($(this), $(this)[0].files[0]);
    uploadFile.hideUploadInput($(this));
    uploadFile.enableButton($(this));
  });

  $(".js-input-file-delete").click(function () {
    uploadFile.removeDataFromFields($(this));
    uploadFile.showUploadInput($(this));
    uploadFile.disableButton($(this));
  });

  $(".js-datepicker").datepicker();
  $.datepicker.setDefaults($.extend($.datepicker.regional["ru"]));

  $(".js-datepicker-icon").click(function () {
    $(this).siblings(".js-datepicker").datepicker("show");
  });

  $(".js-input-autocomplete").autocompleter(dictionaryForAutocomplete, optionsForAutocomplete);

  $(".js-input-add-field").click(function () {
    cloneInput.add($(this));
  });

  $("body").on("click", ".js-input-remove-field", function () {
    cloneInput.remove($(this));
  });

  $('.js-tabs-header-item').click(function () {
    tabs.enter($(this));
  });

  $('.js-widjet-accordion-arrow').click(function () {
    accordion.init($(this));
  });

  $('.js-beauty-arrow-input').focus(function () {
    beautyArrow.focus($(this));
  });
  $('.js-beauty-arrow-input').blur(function () {
    beautyArrow.blur($(this));
  });

  $('.js-show-autocomplete-results').click(function () {
    $(this).siblings('.js-input-autocomplete').focus().autocompleter().showResults(true);
  });

  $('.js-open-popup').click(function () {
    var target = $(this).attr('data-popup');
    popup.showPopup(target);
  });

  $('.js-popup-item').click(function (e) {
    e.stopPropagation();
  });

  $('.js-popup-close, .js-popup').click(function () {
    popup.hidePopups();
  });

  $('.js-input-clear').click(function () {
    $(this).prev().val('');
  });

  $('.main__right-column-title').hover(function () {
    var value = $(this).attr('data-title');
    if (value && value.length > 45) {
      var boundingClientRect = $(this)[0].getBoundingClientRect();
      var style = {
        left: boundingClientRect.left + 'px',
        top: boundingClientRect.top + boundingClientRect.height + 8 + 'px'
      };
      $('body').append('<div class="tooltip js-tooltip" style="top:' + style.top + '; left:' + style.left + ';">' + value + '</div>');
    }
  }, function () {
    $('.js-tooltip').remove();
  });
});