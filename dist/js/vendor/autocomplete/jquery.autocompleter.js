/*
.......................................................
.                Autocompleter (Suggester)            .
.                ������ �� 23.11.2014                 .
.                                                     .
.                  (C) By Protocoder                  .
.                     protocoder.ru                   .
.                                                     .
. ���������������� �� �������� Creative Commons BY-NC .
.   http://creativecommons.org/licenses/by-nc/3.0/    .
.......................................................

������� �� http://www.pengoworks.com/workshop/jquery/autocomplete.htm
*/

/*
��������
var ac = $ ( �������� ���������� ���� ).autocompleter( data, options );


data			URL ��� �������� ������ ���� ������ ������
options			�������������� ���������:
inputClass		��� ������ ��� ���� �����
resultsClass	��� ������ ��� ����� ����������
loadingClass	��� ������ ��� �������� ����������� � �������
selectedClass	��� ������ ����� ���� � ��������� ���������
minChars		���-�� ��������, ����� ������� ���� ����� (�� ��������� 1)
delay			����� �������� � ������������� ����� ������� ������� � �� ������� ������ (�� ��������� 400)
cacheLength		������������ ����� ���������� ����������� ������ (���� 0 - ����������� ����� ���������, -1 - ����������, �� ��������� 10)
maxItemsToShow	������������ ���-�� ����������� � ������ (���� -1 - ��� �����������, �� ��������� -1)
mustMatch		���� true, � ������� �� ������, ����� ������ ������ ��� ���������
extraParams		������, - �������������� ��������� ��� ������� �� ������
width			������ ����������� ������ (���� 0 - �������������� ������������� = ������ ���� �����, �� ��������� 0)
height			������������ ������ ����������� ������, ���� ��������� ������ - ���������� ��������� (���� 0 - �������������, �� ��������� 0)
highlightFirst	���� true ��� ��������� �����������, ������ �� ��� ������������� �������������� (�� ��������� false)
msgNotFound		���������, ������������, ���� ������ �� ������� (�� ��������� not found...)
msgServerError	���������, ������������ ��� ������ ������� (�� ��������� server error...)
msgError		���������, ������������ ��� ������ ������� (�� ��������� error...)
parseData		������� �������������� ������� ������
formatItem		������� ������������ ���� �������� ������
eventsHandler	������� ��������� �������


������ ������	
������, ������������ ��� JSON, ��� ����������� � ���������� ��� ������ ������ ���� � ���������� ����������� ���� ������:

[
	{
		text:		"�������� ��������1",
		pattern:	[ "�������1", "�������2" ... ]
	},
	{
		text:		"�������� ��������2",
		pattern:	[ "�������1", "�������2" ... ]
	},
	{
		text:		"�������� ��������3",
		pattern:	[ "�������1", "�������2" ... ]
	}
]

���� � �������� ������������ �������������� ����, ����� ���������� ����������� - ��� ���������� � ��� � �� ����� ����� ����� ������������.

������ �� ������ ���� ���� POST �� ������� � ���������� q.



������� �������������� ������� ������	
function parseData( items ) { ... return items; }

items	������� ������



������� ������������ ���� �������� ������	
function formatItem( container, item, itemNumber, items, itemsLen ) {
	...
	return {
		html: (null ��� ������ ��� DOM ������), - html / DOM ������ ������� �������� ������,
		value: ������, - ��������, ���������� ��� ������ � ���� �����
	};
}

container	DOM ������ � ������� ����� ��������� ������� � ��� ������
item		������ ��������
itemNumber	���������� ����� �������� � ������
items		������ ���� ���������
itemsLen	���������� ���� ���������

������ ���������� ������ � ������:

html	HTML ������� �������� (��� null, ���� �� �����)
title	�������� title ��� ������� �������� (��� null, ���� �� �����)
value	��������, ������������ � ���� ����� ��� ������ ������� ��������



������� ��������� �������	
function eventsHandler( event, obj, item ) { ... }

�������� event:

over / out		��������� / ������ ��������� � �������� � ������ (obj - DOM ��������, item - ������ ��������)
select			����� �������� � ������ (obj - DOM ��������, item - ������ ��������)
unselect		������ ������ �������� (obj - null, item - ������ ��������)
show / hide		����������� / ������� ������ (obj - DOM ������)
beforeRequest	���������� �� ����, ��� ������ ������ �� ����� (obj - options, item - options.extraParams)



������: �������� ��������
ac.get();

���������� ������ � ����� ������� value - ��� ��, ��� �������� ������ ��������, � selected - ��� ��, ��� ���� ������� �� ������ (����� ���� null, ���� ������ �� ��������)

��������� ������� ����� ��������, ����������� � �������� data-selected ���������� ����, �� ������� ��� ���������� �������������.



������: ���������� ��������
ac.set( value, item, triggerEvent );

value			����� ������ ��������
item			������ ���������������� ��������
triggerEvent	true - ��� ��������� ������� ��������� (����� ���� ������, �� ��������� false)

���� ������� ��� ���������, ������� ������ ���������.

��� "�������" ��������������� �������� undefined ��� null.



������: �������� ���
ac.clearCache();

������� ���� �����������.



������: ���������� ��������� �������
ac.setExtraParams(
	{
		parameter1: value,
		parameter2: value,
		parameter3: value,
		...
	}
);

������������� ���������, ������ ��� ajax-�������, ������������ options.extraParams, ���� ��� �����.



������: �������� ������
ac.showResults( status, timer );

�������� / �������� ���������� ������ �����������.

status	true - �������� / false - ��������
timer	true - �������� ������ �� ����������� ���������



������ ���������� �������� DOM
������ ���������� �������� ����� ��������, ����������� � �������� data-selected ���������� ����, �� ������� ��� ���������� �������������.

������ �������� � ������ �������� � ��� DOM (li) ��������� data-item.



��� ��������� � �������?
���� ���:
var ac = $( �������� ���������� ���� ).autocompleter( ... )
ac.�����(...);


���� ���:
$( �������� ���������� ���� ).autocompleter().�����(...);
*/

jQuery.autocompleter = function ( input, options ) {
	var self = this;

	var timeout = null;
	var hasFocus = false;

	var selectedFieldText = null;

	var cache = {};

	var $input = $( input );

	$input.attr( "autocomplete", "off" );

	if ( options.inputClass ) $input.addClass( options.inputClass );

	var results = document.createElement( "div" );
	var $results = $( results );
	$results.hide().addClass( options.resultsClass ).css(
		{
			"position": "absolute",
			"z-index":	options.zIndex
		}
	);
	if ( "width" in options ) $results.css( "width", options.width );

	$( "body" ).append( results );

	input.autocompleter = self;

	input["data-selected"] = null;

	clearCache();

	if ( options.data ) {
		options.data = parseData( options.data );
		options.url = null;
		options.cacheLength = 0;
	}

	var hasKeyDown = false;
	$input.keydown(
		function ( e ) {
			hasKeyDown = true;

			switch ( e.keyCode ) {
				case 16: //shift
				case 17: //ctrl
				case 18: //alt
				case 35: //end
				case 36: //home
				case 20: //caps lock
				case 45: //insert
				break;

				case 27: //esc
					if ( $results.is( ":visible" ) ) {
						e.preventDefault();
						hideResultsNow();
						return false;
					}

					hideResultsNow();
				break;

				case 38: // up
					e.preventDefault();
					moveSelect( -1 );
				break;

				case 40: // down
					e.preventDefault();
					if ( $results.is( ":visible" ) ) moveSelect( 1 );
					else {
						out();
						oldVal = null;
						onChange();
					}
				break;

				case 9:  // tab
				case 13: // return
					if ( selectCurrent() ) {
						//$input.get( 0 ).blur();
						e.preventDefault();
					}
				break;

				default:
					if ( timeout ) clearTimeout( timeout );
					timeout = setTimeout(
						function () {
							onChange();
						},
						options.delay
					);
					break;
			}
		}
	)
	.keyup(
		function() {
			updateField();
		}
	)
	.focus(
		function () {
			hasFocus = true;
		}
	)
	.blur(
		function () {
			hasFocus = false;

			oldVal = null;

			updateField();

			hideResults(
				function() {
					if ( options.mustMatch && !input["data-selected"] ) {
						if ( input.value != "" ) { input.value = ""; $input.change(); }
					}
				}
			);
		}
	)
	.on (
		"input",
		function() {
			if ( hasKeyDown ) {
				hasKeyDown = false;
				return;
			}

			if ( timeout ) clearTimeout( timeout );
			timeout = setTimeout(
				function () {
					onChange();
				},
				options.delay
			);
		}
	);

	hideResultsNow();

	var mouseDisabled = false;
	var ddmTID = null;
	var ddm = function() {
		mouseDisabled = false;
		ddmTID = null;
	};
	function disableMouse() {
		if ( ddmTID ) clearTimeout( ddmTID );
		mouseDisabled = true;
		ddmTID = setTimeout( ddm, 50 );
	}

	function dataToDom( items ) {
		var ul = document.createElement( "ul" );
		if ( "height" in options ) {
			$( ul ).css( {
				"overflow": "auto",
				"overflow-x": "hidden",
				"max-height": options.height
			} );
		}

		var num = items.length;

		if ( ( options.maxItemsToShow > 0 ) && ( options.maxItemsToShow < num ) ) num = options.maxItemsToShow;

		for ( var i = 0; i < num; i++ ) {
			var item = items[i];
			if ( !item ) continue;

			var li = document.createElement( "li" );
			li["data-item"] = item;
			if ( options.formatItem ) {
				var r = options.formatItem( li, item, i, items, num );
				if ( r.html !== null ) {
					if ( typeof( r.html ) === "string" ) li.innerHTML = r.html;
					else li.appendChild( r.html );
				}
				li["data-value"] = r.value;
			} else {
				li.innerHTML = item.text;
				li["data-value"] = item.text;
			}

			ul.appendChild( li );

			$( li ).hover(
				function( e ) {
					if ( mouseDisabled ) return;

					over( this );
				},
				function( e ) {
					if ( mouseDisabled ) return;

					out();
				}
			)
			.click(
				function( e ) {
					e.preventDefault();
					e.stopPropagation();
					selectItem( this )
					focus( input );
				}
			);
		}

		return ul;
	}

	function updateField( force ) {
		if ( force || ( selectedFieldText && selectedFieldText != input.value ) ) {
			selectedFieldText = null;
			var item = input["data-selected"] ? input["data-selected"] : null;
			input["data-selected"] = null;
			$input.removeClass( options.selectedClass );

			if ( options.eventsHandler ) options.eventsHandler.call( self, "unselect", null, item );
		}
		else {
			if ( input["data-selected"] !== null ) $input.addClass( options.selectedClass );
			else $input.removeClass( options.selectedClass );
		}
	}

	var oldVal = null;
	function onChange() {
		var v = input.value;

		if ( oldVal === v ) return;
		oldVal = v;

		out();

		updateField();

		if ( v.length >= options.minChars ) {
			match( v );
			return;
		}

		hideResultsNow();
	}

	function moveSelect( step ) {
		disableMouse();

		var lis = $( "li", results );
		if ( !lis.size() ) return;

		var active = lis.index( lis.filter( ".acOver" ) );

		var a = active;

		active += step;
		if ( active < 0 ) active = 0;
		else if ( active >= lis.size() ) active = lis.size() - 1;

		if ( a == active ) return;

		over( lis.eq( active ) );

		// ��� IE
		// if ( lis[active] && lis[active].scrollIntoView ) lis[active].scrollIntoView( false );

	}

	function selectCurrent() {
		var li = $( "li.acOver", results )[0];

		if ( li ) {
			selectItem( li );
			return true;
		}

		return false;
	}

	function selectItem( li ) {
		if ( !li ) {
			li = document.createElement( "li" );
			li["data-item"] = null;
			li["data-value"] = null;
		}

		input["data-selected"] = li["data-item"];
		selectedFieldText = li["data-value"];
		input.value = li["data-value"];
		$input.change();
		oldVal = null;

		updateField();

		hideResultsNow();

		if ( options.eventsHandler ) options.eventsHandler.call( self, "select", li, li["data-item"] );

		$results.html( "" );
	}

	function showResults() {
		var pos = $input.offset();

		$results.css( {
			width: "width" in options ? options.width : $input.outerWidth() + "px",
			top: ( pos.top + input.offsetHeight ) + "px",
			left: pos.left + "px"
		} ).show();

		if ( options.eventsHandler ) options.eventsHandler.call( self, "show", results );

		if ( options.highlightFirst ) moveSelect( 1 );
	}

	function over( li ) {
		if ( !li ) return;

		li = $( li );

		if ( li.hasClass( "acOver" ) ) return;

		out();

		li.addClass( "acOver" );

		if ( "height" in options ) scrollIntoView( li[0] );

		if ( options.eventsHandler ) options.eventsHandler.call( self, "over", li[0], li[0]["data-item"] );
	}

	function scrollIntoView( e, c ) {
		if ( !c ) c = e.parentNode;

		var ch = $( c ).innerHeight();
		var cy = parseInt( c.scrollTop, 10 );

		var eh = $( e ).outerHeight();
		var ey = parseInt( e.offsetTop, 10 );

		if ( ey < cy ) c.scrollTop = ey;
		else if ( ey + eh > cy + ch ) c.scrollTop = ey - ch + eh;
	}

	function out( li ) {
		var lis = $( "li", results );

		if ( options.eventsHandler ) {
			if ( !li ) li = lis.filter( ".acOver", results );
			else li = $( li );

			if ( li.size() > 0 ) options.eventsHandler.call( self, "out", li[0], li[0]["data-item"] );
		}

		lis.removeClass( "acOver" );
	}

	function hideResults( func ) {
		out();

		if ( timeout ) clearTimeout( timeout );

		if ( func ) {
			setTimeout(
				function() {
					hideResultsNow();
					func();
				},
				200
			);
			return;
		}

		timeout = setTimeout( hideResultsNow, 200 );
	}

	function hideResultsNow() {
		if ( timeout ) clearTimeout( timeout );

		out();

		if ( $results.is( ":visible" ) ) {
			out();

			$results.hide();

			if ( options.eventsHandler ) options.eventsHandler.call( self, "hide", results );
		}
	}	

	function rebuildResults( items, err ) {
		hideResultsNow();

		results.innerHTML = "";

		if ( err ) {
			var t = document.createElement( "span" );
			t.className = "acError";
			t.innerHTML = err;
			results.appendChild( t );
			t = undefined;
		}
		else if ( items ) results.appendChild( dataToDom( items ) );

		if ( jQuery.autocompleter.isIE() ) {
			// ������� � ��������� IFRAME ��� ���������, ����� ���� ���� SELECT �� ������������ ������ ���������
			$results.append( document.createElement( 'iframe' ) );
		}

		// ���� ���� �� � ������, ��� ������ �� ������� - ������� ��� ������ ����������
		if ( !hasFocus || ( items && items.length == 0 ) ) return;

		showResults();
	}

	function clearCache() {
		cache = {
			length: 0,
			named: {}
		};
	}

	function addToCache( q, items ) {
		if ( !q || !options.cacheLength ) return;

		if ( cache.length >= options.cacheLength ) clearCache();

		cache.named[q] = items;
		cache.length++;
	}

	function getFromCache( q ) {
		if ( options.cacheLength && cache.named.hasOwnProperty( q ) ) return cache.named[q];

		return null;
	}

	function parseData( data ) {
		if ( options.parseData ) return options.parseData( data );

		return data;
	}

	function match( q ) {
		q = $.trim( q );

		if ( q < options.minChars ) return;

		if ( options.cacheLength ) {
			var items = getFromCache( q );
			if ( items !== null ) {
				if ( items.length > 0 ) rebuildResults( items );
				else rebuildResults( null, options.msgNotFound );
				return;
			}
		}

		hideResultsNow();

		if ( options.eventsHandler ) options.eventsHandler.call( self, "beforeRequest", options, options.extraParams );

		if ( options.data ) {
			items = matchArr( q, options.data );

			if ( items ) {
				addToCache( q, items );
				if ( items.length > 0 ) rebuildResults( items );
				else rebuildResults( null, options.msgNotFound );
			}
			else rebuildResults( null, options.msgError );

			return;
		}

		$input.addClass( options.loadingClass );

		options.extraParams.q = q;

		$.ajax(
			{
				type: "POST",
				dataType: "json",
				url: options.url,// + "?q=" + encodeURI( q ),
				data: options.extraParams,
				cache: false,
				success: function( data ) {
					$input.removeClass( options.loadingClass );

					var items = parseData( data );
					if ( items ) {
						addToCache( q, items );
						if ( items.length > 0 ) rebuildResults( items );
						else rebuildResults( null, options.msgNotFound );
					}
					else rebuildResults( null, options.msgError );
				},
				error: function() {
					$input.removeClass( options.loadingClass );

					rebuildResults( null, options.msgServerError );
				}
			}
		);
	}

	function matchArr( q, items ) {
		var mItems = [];

		if ( q ) {
			q = q.toLowerCase();
			var iL = items.length;
			for ( var i = 0; i < iL; i++ ) {
			 	var item = items[i];
			 	for ( var j = item.pattern.length - 1; j >= 0; j-- ) {
			 		if ( item.pattern[j].toLowerCase().indexOf( q ) >= 0 ) {
			 			mItems.push( item );
			 			break;
			 		}
			 	}
			}
		}
		else mItems = items;

		return mItems;
	}

	function focus( el ) {
		var x, y;
		if ( typeof( window.pageXOffset ) !== 'undefined' ) {
			x = window.pageXOffset;
			y = window.pageYOffset;
		} else if ( typeof( window.scrollX ) !== 'undefined' ) {
			x = window.scrollX;
			y = window.scrollY;
		} else if ( document.documentElement && typeof( document.documentElement.scrollLeft ) !== 'undefined' ) {
			x = document.documentElement.scrollLeft;
			y = document.documentElement.scrollTop;
		} else {
			x = document.body.scrollLeft;
			y = document.body.scrollTop;
		}

		el.focus();

		if ( typeof( x ) !== 'undefined' ) {
			setTimeout( function() { window.scrollTo( x, y ); }, 100 );
		}
	}

	this.set = function( value, item, triggerEvent ) {
		oldVal = null;

		if ( !arguments.length ) {
			input.value = "";
			$input.change();
			updateField( true );
			hideResultsNow();
			$results.html( "" );
			return;
		}

		input["data-selected"] = item;
		selectedFieldText = value;
		input.value = value;
		$input.change();

		updateField();

		hideResultsNow();

		if ( triggerEvent && options.eventsHandler ) {
			var li = document.createElement( "li" );
			li["data-item"] = item;
			li["data-value"] = value;
			options.eventsHandler.call( self, "select", li, item );
		}

		$results.html( "" );

		return $input;
	};

	this.get = function() {
		return {
			selected: input["data-selected"] ? input["data-selected"] : null,
			value: input.value
		};
	};

	this.clearCache = function() {
		clearCache();

		return $input;
	};

	this.setExtraParams = function ( p ) {
		options.extraParams = p;

		return $input;
	};

	this.showResults = function( status, timer ) {
		oldVal = null;

		if ( timer == undefined ) timer = !!timeout;
		hideResultsNow();

		if ( timer ) {
			timeout = setTimeout(
				function () {
					onChange();
				},
				options.delay
			);
		}

		if ( status ) onChange();

		return $input;
	};


	return $input;
};

jQuery.autocompleter.isIE = ( function() {
	var v = 3, div = document.createElement( "div" );

	while (
		div.innerHTML = '<!--[if gt IE '+(++v)+']><i></i><![endif]-->',
		div.getElementsByTagName('i')[0]
	) {}

	return new Function( "return " + ( v> 4 ? v : "false" ) + ";" );
} )();

jQuery.fn.autocompleter = function ( url, options, data ) {
	if ( arguments.length == 0 ) {
		if ( this[0].autocompleter ) return this[0].autocompleter;
		$.error( "Wrong parameters for autocompleter" );
		return;
	}

	options = options || {};

	if ( $.isArray( url ) ) {
		options.data = url;
		url = null;
	}
	options.url = url;

	options.inputClass = options.inputClass || "acInput";
	options.resultsClass = options.resultsClass || "acResults";
	options.loadingClass = options.loadingClass || "acLoading";
	options.selectedClass = options.selectedClass || "acSelected";

	if ( !( "zIndex" in options ) ) options.zIndex = 10000;

	if ( !( "minChars" in options ) ) options.minChars = 1;
	options.delay = options.delay || 400;

	options.mustMatch = options.mustMatch || false;

	options.maxItemsToShow = options.maxItemsToShow || -1;

	if ( !( "cacheLength" in options ) ) options.cacheLength = 10;
	if ( !( "extraParams" in options ) ) options.extraParams = {};

	options.highlightFirst = options.highlightFirst || false;

	options.msgNotFound = options.msgNotFound || "not found";
	options.msgError = options.msgError || "error";
	options.msgServerError = options.msgServerError || "server error";

	this.each( function () {
		var input = this;
		new jQuery.autocompleter( input, options );
	} );

	return this;
};
