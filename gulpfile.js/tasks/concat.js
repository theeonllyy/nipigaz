var
	gulp = require('gulp'),
	$ = require('gulp-load-plugins')(),

	config = require('../config'),
	consoleError = require('../utils/console_error');

function concat() {
	return gulp.src( config.paths.built.styles.all )
		.pipe( $.concat('all.css') )
		.pipe( gulp.dest( config.paths.built.styles.path ) );
};

module.exports = concat;
