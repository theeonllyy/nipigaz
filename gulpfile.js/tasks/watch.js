var
	gulp = require('gulp'),
	$ = require('gulp-load-plugins')(),

	config = require('../config');

function watch() {
	gulp.watch( config.paths.src.scripts.all, gulp.series('scripts') );
	gulp.watch( [config.paths.src.styles.base, config.paths.src.styles.blocks], gulp.series('styles') );
	gulp.watch( config.paths.src.images.all, gulp.series('images') );
	gulp.watch( config.paths.src.sprites.images.all, gulp.series('sprites') );
	gulp.watch( config.paths.src.templates.all, gulp.series('templates') );
};

module.exports = watch;
