var
	gulp = require('gulp'),
	$ = require('gulp-load-plugins')(),

	config = require('../config'),
	consoleError = require('../utils/console_error');

function fonts() {
	return gulp.src( config.paths.src.fonts.all )
		.pipe( gulp.dest( config.paths.built.fonts.path ) );
};

module.exports = fonts;
