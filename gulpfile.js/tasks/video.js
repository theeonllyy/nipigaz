var
	gulp = require('gulp'),
	$ = require('gulp-load-plugins')(),

	config = require('../config'),
	consoleError = require('../utils/console_error');

function video() {
	return gulp.src( config.paths.src.video.all )
		.pipe( gulp.dest( config.paths.built.video.path ) );
};

module.exports = video;
